# @fnet/pick-version

The **@fnet/pick-version** project focuses on offering a versatile and efficient solution for scanning a given set of software versions, finding matches based on a specified version pattern, and selecting the most recent version that aligns with the given pattern.

## Key Features 

1. **Selection of Most Recent Version**: By providing a list of versions and an optional pattern, the project's central functionality allows it to sift through all the available versions and pick the most recent version that fits the supplied pattern. In the absence of a pattern, it selects the latest available version from the list.

2. **Pattern Matching**: This application utilizes the power of SemVer versioning to match the given pattern effectively. The user can provide version constraints like '^1.0.0' or '~2.1.2' to finely filter the versions. 

3. **Error Handling**: In situations where incorrect inputs are supplied, the project is designed to return specific, understandable error messages. This might occur when the versions are not provided in an array of strings format or if the provided pattern is not a string.

4. **Sorting and Filtering**: It performs sort and filter operations on the versions list to ensure the end result is always the most recent matching version, it still provides a response (null) even when no matching versions are found.

In summary, @fnet/pick-version is a user-focused project, designed to help end-users effectively sift through versions, according to specific patterns with the resulting output being the latest version matching the pattern or null if no matches are found.