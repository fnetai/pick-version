import semver from 'semver';

/**
 * Selects the most recent version that matches the given pattern from a list of versions.
 * 
 * @param {Object} args - The arguments object.
 * @param {string[]} args.versions - The list of versions to consider.
 * @param {string} args.pattern - The version pattern to match (e.g., '^1.0.0', '~2.1.2').
 * 
 * @returns {string|null} The most recent matching version, or null if no match is found.
 */
export default async ({ versions, pattern }) => {
  // Ensure the input is valid
  if (!Array.isArray(versions) || versions.some(v => typeof v !== 'string')) {
    throw new Error('The "versions" argument must be an array of strings.');
  }
  if (typeof pattern !== 'undefined' && typeof pattern !== 'string') {
    throw new Error('The "pattern" argument must be a string.');
  }

  // Filter versions that satisfy the pattern
  const matchingVersions = versions.filter(version => pattern ? semver.satisfies(version, pattern) : true);

  // Sort the matching versions in descending order
  matchingVersions.sort(semver.rcompare);

  // Return the first version (the most recent one) or null if there are no matching versions
  return matchingVersions[0] || null;
};